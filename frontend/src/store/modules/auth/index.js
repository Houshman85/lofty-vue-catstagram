import router from '../../../router/index'
const state = {
  isAuthenticated: false,
  user_name: ''
}

const mutations = {
  updateAuth(state) {
    state.isAuthenticated = true
  },
  updateUser(state, name) {
    state.user_name = name
  },
  logout(state) {
    state.isAuthenticated = false,
    state.user_name = ''
  }
}

const actions = {
  updateAuthInfo({ commit }, data) {
    commit('updateAuth')
    commit('updateUser', data.first_name)
  },
  logOut({ commit }) {
    router.push('/login')
    commit('logout')
  }
}

const getters = {
  isAuthenticated: state => state.isAuthenticated,
  user_name: state => state.user_name
}

const authModule = {
  state,
  mutations,
  actions,
  getters
}
export default authModule
