import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate"
import auth from './modules/auth'

const persist = createPersistedState({
  paths: ['auth']
})


export default createStore({
  modules: {
    namespaced: true,
    auth
  },
  plugins: [persist]
})
