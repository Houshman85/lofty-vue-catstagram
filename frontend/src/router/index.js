import {createWebHistory, createRouter} from 'vue-router'
import store from '../store'

const HomePage = () => import('../views/HomePage.vue')
const LoginPage = () => import('../views/LoginPage.vue')
const PhotoPage = () => import('../views/PhotoPage.vue')
const PasswordChange = () => import('../views/PasswordChange.vue')
const RegisterUser = () => import('../views/RegisterUser.vue')
const NotFound = () => import('../views/NotFound.vue')

const routes = [
    {path: '/home', component: HomePage, meta: { requiresAuth: true }},
    {path: '/login', component: LoginPage},
    {path: '/password-change', component: PasswordChange},
    {path: '/register', component: RegisterUser},
    {path: '/image/:photo', component: PhotoPage, meta: { requiresAuth: true }},
    {path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound}
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach((to) => {
    if(to.meta.requiresAuth && store.getters.isAuthenticated === false) {
        return {
            path: '/login',
            //save location here
            query: { redirect: to.fullPath },
        }
    }
})

export default router
