import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'
import axios from 'axios'
import store from './store'
import VueAxios from 'vue-axios'
import Toaster from '@meforma/vue-toaster'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'

const app = createApp(App)

app
.use(router)
.use(store)
.use(VueAxios, {$http: axios})
.use(Toaster)
.mount('#app')
